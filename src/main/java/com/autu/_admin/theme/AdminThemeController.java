package com.autu._admin.theme;

import com.autu.common._config.BlogContext;
import com.autu.common.controller.BaseController;

/**
* @author 作者:张三
* @createDate 创建时间：202x年9月13日 下午1:14:54
*/
public class AdminThemeController extends BaseController  {

	public void index() {
		setAttr("currentTheme", BlogContext.getTheme());
		render("themes.html");
	}
	
}
